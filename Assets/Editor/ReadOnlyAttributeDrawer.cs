﻿// /** 
//  * ReadOnlyAttributeDrawer.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Editor
{
    #region Dependencies

    using Assets.Core;
    using UnityEditor;
    using UnityEngine;

    #endregion

    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyAttributeDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property,
                                                     GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            GUI.enabled = false;
            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }
}