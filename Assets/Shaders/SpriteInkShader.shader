﻿Shader "Wilsk/SpriteInkShader"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_AlphaThreshold("Alpha Threshold", Range(0.,1.)) = 0.8
		_Alpha("Under Threshold Alha", Range(0.,1.)) = 0.0
	}

	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite On
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"

			#define IF(a, b, c) lerp(b, c, step((fixed) (a), 0))

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord  : TEXCOORD0;
			};
		
			fixed4 _Color;
			float _AlphaThreshold;
			float _Alpha;

			v2f vert(appdata_t IN)
			{
				v2f OUT;

				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				OUT.vertex = UnityPixelSnap(OUT.vertex);

				return OUT;
			}

			sampler2D _MainTex;

			fixed4 SampleSpriteTexture(float2 uv)
			{
				fixed4 color = tex2D(_MainTex, uv);
				//color.a = IF(color.r < _AlphaThreshold, _Alpha, color.a);
				return color;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;

				float whiteness = c.r * c.g * c.b;
				float newCa = c.a * IF(whiteness < _AlphaThreshold, _Alpha, 1);
				c.rgb *= c.a * newCa * _Color.a;
				c.a = newCa;

				return c;
			}
			ENDCG
		}
	}
}
