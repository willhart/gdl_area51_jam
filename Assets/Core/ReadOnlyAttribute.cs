﻿// /** 
//  * ReadOnlyAttribute.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core
{
    #region Dependencies

    using UnityEngine;

    #endregion

    public class ReadOnlyAttribute : PropertyAttribute
    {
    }
}