﻿// /** 
//  * Bootstrapper.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core
{
    #region Dependencies

    using System.Collections.Generic;
    using Assets.Core.Data;
    using UnityEngine;
    using UnityEngine.Assertions;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    #endregion

    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private GameObject _spritePrefab;
        [SerializeField] private GameObject _blockerPrefab;
        [SerializeField] private Text _dialogueText;
        private ScenePlayer _currentScene;

        private readonly List<GameObject> _spritePool = new List<GameObject>();
        private static Bootstrapper Instance;
        
        private void Start()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            _currentScene = new ScenePlayer(GameLoader.NextScene(), _spritePrefab, _blockerPrefab, _spritePool, _dialogueText);
            DontDestroyOnLoad(gameObject);
        }

        private void Update()
        {
            if (_currentScene == null)
            {
                var sceneToLoad = GameLoader.CurrentScene;
                if (sceneToLoad == null)
                {
                    Debug.Log("Scenes complete, loading menu");
                    Instance = null;

                    if (_dialogueText != null)
                    {
                        var ddol = _dialogueText.gameObject.GetComponent<DontDestroyOnLoad>();
                        if (ddol != null) ddol.Destroy();
                    }
                    
                    SceneManager.LoadScene("MainMenu");
                    enabled = false;
                    Destroy(gameObject);

                    return;
                }

                Debug.Log(string.Format("Now Loading Scene {0}", sceneToLoad.SceneNumber));
                _currentScene = new ScenePlayer(
                    sceneToLoad,
                    _spritePrefab,
                    _blockerPrefab,
                    _spritePool,
                    _dialogueText);
            }

            _currentScene.Update();

            if (!_currentScene.IsComplete) return;
            _currentScene = null;

            var nextScene = GameLoader.PeekNext;

            if (nextScene == null)
            {
                GameLoader.NextScene();
                Debug.Log("All scenes complete");
                return;
            }

            Debug.Log(string.Format("Scene {0} complete, at {1} moving to scene {2}", GameLoader.CurrentScene.SceneNumber, Time.time, nextScene.SceneNumber));

            if (nextScene.IsMiniGame)
            {
                foreach (var sprite in _spritePool)
                {
                    Destroy(sprite);
                }
                _spritePool.Clear();

                SceneManager.LoadScene("MiniGame");
            }
            else if (SceneManager.GetActiveScene().name == "MiniGame")
            {
                SceneManager.LoadScene("Main");
            }

            GameLoader.NextScene();
        }

        public void ReturnFromMiniGame()
        {
            _currentScene.ForceComplete = true;
        }
    }
}