﻿namespace Assets.Core
{
    using System;
    using UnityEngine;
    using System.Collections;
    using Assets.Core.Audio;

    public class SuperNovaController : MonoBehaviour
    {
        public float MaxScale;
        public float ScaleUpRate;

        private bool _isLoweringVolume = true;

        [SerializeField] private AudioSource _audio;
        [SerializeField] private AudioClip _pingClip;
        private bool _startScaling;
        private bool _scalingUp = true;
        
        public void Start()
        {
            transform.localScale = new Vector3(0, 0, 1);
            _audio = GameObject.FindGameObjectWithTag("AudioSystem").GetComponent<AudioSource>();
            StartCoroutine(SwitchAudio());
        }

        public void Update()
        {
            if (!_startScaling) return;
            var lerpVal = Mathf.MoveTowards(transform.localScale.x, _scalingUp ? MaxScale : 0, ScaleUpRate*Time.deltaTime);
            transform.localScale = new Vector3(lerpVal, lerpVal, 1);

            if (Math.Abs(lerpVal - MaxScale) < float.Epsilon) _scalingUp = false;
        }

        private IEnumerator SwitchAudio()
        {
            while (true)
            {
                _audio.volume = Mathf.MoveTowards(
                    _audio.volume,
                    _isLoweringVolume ? 0 : 0.6f,
                    _isLoweringVolume ? 0.01f : 0.2f);

                if (_audio.volume <= 0 && _isLoweringVolume)
                {
                    _isLoweringVolume = false;
                    _audio.gameObject.GetComponent<ProceduralAudioPlayer>().enabled = false;
                    _audio.Stop();
                    _audio.clip = _pingClip;
                }

                if (_audio.volume > 0.55f && !_isLoweringVolume)
                {
                    _startScaling = true;
                    _audio.PlayOneShot(_pingClip);
                    yield break;
                }

                yield return null;
            }

        }
    }
}