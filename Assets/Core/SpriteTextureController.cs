﻿// /** 
//  * SpriteTextureController.cs
//  * Will Hart
//  * 20161004
// */

namespace Assets.Core
{
    #region Dependencies

    using System.Collections.Generic;
    using UnityEngine;

    #endregion

    public class SpriteTextureController : MonoBehaviour
    {
        [SerializeField] private List<Texture> _textures;
        [SerializeField] private float _texturePeriod;

        private SpriteRenderer _renderer;
        private int _currentTexture;
        private float _counter = 0;

        private void Awake()
        {
            if(_textures.Count == 0)
                Debug.LogError("Sprite Texture Controller must have some textures assigned in the inspector");

            _renderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            _counter -= Time.deltaTime;
            if (_counter > 0) return;

            _renderer.material.SetTexture("_NoiseTexture", _textures[_currentTexture++]);
            if (_currentTexture >= _textures.Count) _currentTexture = 0;

            _counter = _texturePeriod;
        }
    }
}