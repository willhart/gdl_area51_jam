﻿// /** 
//  * VectorExtensions.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core
{
    using UnityEngine;

    public static class VectorExtensions
    {
        public static Vector2 ToVec2(this Vector3 vec)
        {
            return new Vector2(vec.x, vec.y);
        }

        public static Vector3 ToVec3(this Vector2 vec)
        {
            return new Vector3(vec.x, vec.y, 0);
        }
    }
}