﻿namespace Assets.Core
{
    using System;
    using UnityEngine;

    public class DontDestroyOnLoad : MonoBehaviour
    {
        private static GameObject _instance;

        // Use this for initialization
        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            _instance = gameObject;
        }

        internal void Destroy()
        {
            _instance = null;
            Destroy(gameObject);
        }
    }
}