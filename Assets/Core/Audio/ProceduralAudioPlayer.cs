﻿// /** 
//  * ProceduralAudioPlayer.cs
//  * Will Hart
//  * 20161005
// */

namespace Assets.Core.Audio
{
    #region Dependencies

    using System.Collections.Generic;
    using UnityEngine;

    #endregion

    [RequireComponent(typeof(AudioSource))]
    public class ProceduralAudioPlayer : MonoBehaviour
    {
        [SerializeField, Range(1, 240)] private int _beatsPerMinute;
        [SerializeField] private List<string> _musicSeeds;
        [SerializeField] private int _maxSequenceLength;
        [SerializeField, Range(0.0f, 0.5f)] private float _gain = 0.05f;

        private MusicGenerator _music;
        private LSystem _lSystem;

        private void Awake()
        {
#if UNITY_WEBGL
            // this is necessary because webgl audio support is "poor"
            var clip = Resources.Load<AudioClip>("WebGLSounds/webgl_music");
            var source = GetComponent<AudioSource>();
            source.clip = clip;
            source.loop = true;
            source.Play();
            enabled = false;
#else
            _lSystem = new LSystem(
                new Dictionary<char, string>
                {
                    {'a', "a1b1d1_"},
                    {'b', "e1f1A1_"},
                    {'c', "D3_1D1_1F3_"},
                    {'d', "A1f1e1_"},
                    {'e', "c1_1c1_1d1c1_"},
                    {'f', "b1_1b1_1b1_"},
                    {'g', "e1_"},

                    {'A', "B1d1_1g1_1A"},
                    {'B', "_"},
                    {'C', "G1_"},
                    {'D', "g1A1B1C1_"},
                    {'E', "e"},
                    {'F', "E1_1E1_1E1_"},
                    {'G', "e"},

                    {'_', "A"},
                    {'1', "2"},
                    {'2', "1"},
                    {'3', "2"},
                    {'4', "1"},
                    {'5', "3"}
                },
                _maxSequenceLength);

            var seed = _musicSeeds[Random.Range(0, _musicSeeds.Count)];
            _music = new MusicGenerator(_lSystem.Start(seed), _beatsPerMinute, _gain);
#endif
        }

        private void Update()
        {
            _music.IncrementTime(Time.deltaTime);
        }

        private void OnAudioFilterRead(IList<float> data, int channels)
        {
            _music.Update(data, channels);
        }
    }
}