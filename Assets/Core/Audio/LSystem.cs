﻿// /** 
//  * LSystem.cs
//  * Will Hart
//  * 20161005
// */

namespace Assets.Core.Audio
{
    #region Dependencies

    using System;
    using System.Collections.Generic;
    using System.Text;

    #endregion

    public class LSystem
    {
        private static readonly Random _rand = new Random();
        private readonly int _maxSequenceLength;
        private readonly Dictionary<char, string> _replacements;

        public LSystem(Dictionary<char, string> replacements, int trimLength)
        {
            _replacements = replacements;
            _maxSequenceLength = trimLength;
        }

        private string CurrentValue { get; set; }

        public IEnumerator<string> Start(string initial)
        {
            CurrentValue = initial;
            return GetEnumerator();
        }

        private void Iterate()
        {
            if (CurrentValue.Length > _maxSequenceLength)
            {
                // reset to just the first character if we reach the sequence length limit
                CurrentValue = CurrentValue[0].ToString();
                return;
            }

            if (CurrentValue.Length < 16 && _rand.NextDouble() > 0.75)
            {
                // randomly repeat short phrases
                return;
            }

            var gp = GetPairs();
            var sb = new StringBuilder();

            while (gp.MoveNext())
            {
                var thisChar = gp.Current;
                sb.Append(GetSubstitute(thisChar[0]));
                sb.Append(GetSubstitute(thisChar[1]));
            }

            CurrentValue = sb.ToString();
            UnityEngine.Debug.Log(CurrentValue);
        }

        private string GetSubstitute(char current)
        {
            return _replacements.ContainsKey(current) ? _replacements[current] : current.ToString();
        }

        private IEnumerator<string> GetEnumerator()
        {
            while (true)
            {
                var gp = GetPairs();
                while (gp.MoveNext())
                {
                    yield return gp.Current;
                }

                Iterate();
            }
        }

        private IEnumerator<string> GetPairs()
        {
            var current = new StringBuilder(2);

            foreach (var ch in CurrentValue)
            {
                // strings must be length 2, the tone and the number of beats
                if (current.Length == 1)
                {
                    yield return current.Append(ch).ToString();
                    continue;
                }

                current.Length = 0;
                current.Append(ch);
            }
        }
    }
}