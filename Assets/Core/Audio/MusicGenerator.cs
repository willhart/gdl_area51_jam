﻿// /** 
//  * MusicGenerator.cs
//  * Will Hart
//  * 20161005
// */

namespace Assets.Core.Audio
{
    #region Dependencies

    using System.Collections.Generic;
    using UnityEngine;

    #endregion

    public class MusicGenerator
    {
        private static readonly Dictionary<char, int[]> Frequencies = new Dictionary<char, int[]>
        {
            {'a', new[] {110}},
            {'b', new[] {123}},
            {'c', new[] {131}},
            {'d', new[] {147}},
            {'e', new[] {165}},
            {'f', new[] {175}},
            {'g', new[] {196}},
            {'A', new[] {220}},
            {'B', new[] {247}},
            {'C', new[] {262}},
            {'D', new[] {294}},
            {'E', new[] {330}},
            {'F', new[] {349}},
            {'G', new[] {392}},
            {'_', new[] {1}}
        };

        private readonly float _beatLength;
        private readonly float _gain;
        private readonly IEnumerator<string> _generator;

        private char _currentTone;

        private float _phase;
        private readonly float _samplingFrequency;
        private float _timeRemaining = -1;

        public MusicGenerator(IEnumerator<string> generator, int beatsPerMinute, float gain)
        {
            _generator = generator;
            _gain = gain;
            _beatLength = 60f/beatsPerMinute;
            _samplingFrequency = AudioSettings.outputSampleRate;
        }

        public void Update(IList<float> data, int channels)
        {
            DetermineNote();

            var numTones = Frequencies[_currentTone].Length;

            foreach (var tone in Frequencies[_currentTone])
                ProduceTone(data, channels, tone, numTones);
        }

        public void IncrementTime(float deltaTime)
        {
            _timeRemaining -= deltaTime;
        }

        private void DetermineNote()
        {
            if (_timeRemaining > 0) return;

            _generator.MoveNext();
            var nextStep = _generator.Current;

            if (nextStep == null)
                Debug.LogError("Received null from generator");

            _currentTone = nextStep[0];
            _timeRemaining = _beatLength*int.Parse(nextStep.Substring(1, 1));
        }

        private void ProduceTone(IList<float> data, int channels, int tone, float numTones)
        {
            var increment = tone*2*Mathf.PI/_samplingFrequency;

            for (var i = 0; i < data.Count; i = i + channels)
            {
                _phase = _phase + increment;
                data[i] += _gain*(GetWave(_phase)/numTones);
                    ;
                if (channels == 2) data[i + 1] = data[i];

                if (_phase > 2*Mathf.PI) _phase = 0;
            }
        }

        private static float GetWave(float phase)
        {
            return SineWave(phase);
        }

        private static float SineWave(float phase)
        {
            return Mathf.Sin(phase);
        }
    }
}