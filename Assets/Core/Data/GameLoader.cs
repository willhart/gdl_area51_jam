﻿// /** 
//  * GameLoader.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core.Data
{
    #region Dependencies

    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using UnityEngine;

    #endregion

    public static class GameLoader
    {
        private static readonly List<SceneDescription> Scenes = new List<SceneDescription>();
        private static readonly Dictionary<string, Sprite> Sprites = new Dictionary<string, Sprite>();

        private static int _scene;
        private static readonly Sprite TestSprite;

        static GameLoader()
        {
            ReloadGame();

            TestSprite = GetSprite("test_sprite");
        }

        public static void ReloadGame()
        {
            Scenes.Clear();

            var textAssets = Resources.LoadAll<TextAsset>("Levels/");

            foreach (var text in textAssets)
            {
                if (!text.name.StartsWith("Scene")) continue;

                Debug.Log("Loading in JSON Scene: " + text.name);
                Scenes.Add(JsonUtility.FromJson<SceneDescription>(text.text));
            }

            Scenes.Sort((a, b) => a.SceneNumber.CompareTo(b.SceneNumber));
        }

        public static SceneDescription NextScene()
        {
            CurrentScene = _scene >= Scenes.Count ? null : Scenes[_scene];
            _scene++;
            return CurrentScene;
        }

        public static void Restart()
        {
            _scene = 0;
        }

        public static Sprite GetSprite(string spriteName)
        {
            if (!Sprites.ContainsKey(spriteName))
            {
                var newSprite = Resources.Load<Sprite>("Sprites/" + spriteName);
                Sprites.Add(spriteName, newSprite);
            }

            if (Sprites[spriteName] != null) return Sprites[spriteName];

            Debug.LogWarning(string.Format("Sprite {0} does not exist, displaying test sprite", spriteName));
            return TestSprite;
        }

        public static ReadOnlyCollection<SceneDescription> SceneData
        {
            get { return Scenes.AsReadOnly(); }
        }

        public static SceneDescription CurrentScene { get; private set; }

        public static SceneDescription PeekNext
        {
            get
            {
                return _scene < Scenes.Count ? Scenes[_scene] : null;
            }
        }
    }
}