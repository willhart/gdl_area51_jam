﻿// /** 
//  * SceneDescription.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core.Data
{
    #region Dependencies

    using System;
    using System.Collections.Generic;

    #endregion

    [Serializable]
    public class SceneDescription
    {
        public int SceneNumber;

        public bool IsMiniGame;
        public int MiniGameNumber;
        
        public bool HasNova;
        public float NovaStartTime;
        
        public List<SpriteDescription> Sprites;
        public List<BlockerDescription> Blockers;
        public List<TextDescription> TextLines;
    }
}