﻿// /** 
//  * TextDescription.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core.Data
{
    #region Dependencies

    using System;

    #endregion

    [Serializable]
    public class TextDescription
    {
        public string TextLine;
        public float DisplayStartTime;
        public float DisplayEndTime;
    }
}