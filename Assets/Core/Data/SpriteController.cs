﻿// /** 
//  * SpriteController.cs
//  * Will Hart
//  * 20161006
// */

namespace Assets.Core.Data
{
    #region Dependencies

    using System;
    using System.Collections;
    using UnityEngine;

    #endregion

    public class SpriteController
    {
        private readonly GameObject _container;
        private readonly SpriteDescription _sprite;
        private readonly Material _mainMaterial;
        private readonly bool hasMovement;

        private readonly bool hasRotation;

        public SpriteController(SpriteDescription sprite, GameObject container)
        {
            _sprite = sprite;
            _container = container;

            var sr = _container.GetComponent<SpriteRenderer>();
            sr.sprite = GameLoader.GetSprite(_sprite.SpriteName);
            _container.transform.position = _sprite.InitialPosition;
            _container.transform.rotation = Quaternion.Euler(new Vector3(0, 0, _sprite.InitialRotation));

            _mainMaterial = sr.material;

            hasRotation = Math.Abs(_sprite.InitialRotation - _sprite.FinalRotation) > float.Epsilon;
            hasMovement = (_sprite.InitialPosition - _sprite.FinalPosition).sqrMagnitude > 1;
        }

        public void Update(float elapsedTime)
        {
            if ((_sprite == null) || (_container == null)) return;
            _container.SetActive((elapsedTime > _sprite.DisplayStartTime) && (elapsedTime < _sprite.DisplayEndTime));

            HandleFading(elapsedTime);
            
            if ((elapsedTime < _sprite.AnimationStartTime) || (elapsedTime > _sprite.AnimationEndTime)) return;

            if (!(hasMovement || hasRotation)) return;

            var percentage = (elapsedTime - _sprite.AnimationStartTime)/
                             (_sprite.AnimationEndTime - _sprite.AnimationStartTime);

            if (hasMovement)
                _container.transform.position = Vector3.Lerp(_sprite.InitialPosition, _sprite.FinalPosition, percentage);

            if (hasRotation)
                _container.transform.rotation =
                    Quaternion.Euler(
                        new Vector3(0, 0, Mathf.Lerp(_sprite.InitialRotation, _sprite.FinalRotation, percentage)));
        }

        private void HandleFading(float elapsedTime)
        {
            if (elapsedTime < _sprite.FadeInDuration)
            {
                SetTintAlpha(Mathf.Lerp(0, 1, elapsedTime / _sprite.FadeInDuration));
            }
            else if (elapsedTime > _sprite.DisplayEndTime - _sprite.FadeOutDuration)
            {
                SetTintAlpha(Mathf.Lerp(0, 1, (_sprite.DisplayEndTime - elapsedTime)/_sprite.FadeOutDuration));
            }
            else
            {
                SetTintAlpha(1);
            }
        }

        private void SetTintAlpha(float current)
        {
            _mainMaterial.SetColor("_Color", new Color(1, 1, 1, current));
        }
    }
}