﻿// /** 
//  * SpriteDescription.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core.Data
{
    #region Dependencies

    using System;
    using UnityEngine;

    #endregion

    [Serializable]
    public class BlockerDescription
    {
        public float DisplayStartTime;
        public float DisplayEndTime;

        public Vector3 Position;
        public float Rotation;
        public Vector3 Scale;

        public GameObject Instance;
    }
}