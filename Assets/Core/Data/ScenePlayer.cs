﻿// /** 
//  * ScenePlayer.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine.UI;

    #region Dependencies

    using UnityEngine;

    #endregion

    public class ScenePlayer
    {
        private readonly SceneDescription _scene;
        private readonly float _startTime;
        private readonly float _endTime;
        private readonly List<SpriteController> _controllers;
        private readonly Text _uiText;
        private readonly GameObject _blocker;
        private readonly SuperNovaController _nova;
        
        public ScenePlayer(SceneDescription scene, GameObject spritePrefab, GameObject blockerPrefab, List<GameObject> spritePool, Text uiText)
        {
            _scene = scene;
            _uiText = uiText;
            _blocker = blockerPrefab;

            _startTime = Time.time;
            _endTime = scene.IsMiniGame ? float.MaxValue : scene.Sprites.Max(o => o.DisplayEndTime) + _startTime;
            
            _controllers = AssignSprites(spritePool, _scene, spritePrefab);

            if (scene.HasNova)
            {
                _nova = GameObject.FindGameObjectWithTag("Nova").GetComponent<SuperNovaController>();
            }

            Debug.Log(string.Format("Built Scene Player for scene {0}", _scene.SceneNumber));
        }

        public bool IsComplete { get { return Time.time > _endTime || ForceComplete; } }

        public bool IsEmpty
        {
            get { return _scene == null; }
        }

        public void Update()
        {
            if (IsEmpty) return;

            UpdateBlockers();
            UpdateSprites();
            UpdateText(_scene);
            UpdateNova();
        }

        private void UpdateNova()
        {
            if (_nova == null) return;

            var now = Time.time;
            _nova.enabled = now > _scene.NovaStartTime + _startTime && _scene.HasNova;
        }

        private void UpdateBlockers()
        {
            var now = Time.time;
            foreach (var blocker in _scene.Blockers)
            {
                if (now > blocker.DisplayStartTime + _startTime && blocker.Instance == null)
                {
                    blocker.Instance = (GameObject)Object.Instantiate(_blocker, blocker.Position, Quaternion.Euler(new Vector3(0, 0, blocker.Rotation)));
                    blocker.Instance.transform.localScale = blocker.Scale;
                }
                else if (now > blocker.DisplayEndTime + _startTime && blocker.Instance != null)
                {
                    Object.Destroy(blocker.Instance);
                    blocker.Instance = null;
                }

            }
        }

        private List<SpriteController> AssignSprites(ICollection<GameObject> spritePool, SceneDescription scene, GameObject spritePrefab)
        {
            while (spritePool.Count < _scene.Sprites.Count)
            {
                spritePool.Add(Object.Instantiate(spritePrefab));
            }
            
            var sprites = scene.Sprites.Select((o, i) => new SpriteController(o, spritePool.ElementAt(i))).ToList();
            
            return sprites;
        }

        private void UpdateText(SceneDescription scene)
        {
            if (_uiText == null) return;

            var elapsed = Time.time - _startTime;
            var sb = new StringBuilder();

            foreach (var textLine in scene.TextLines)
            {
                if (elapsed < textLine.DisplayStartTime || elapsed > textLine.DisplayEndTime) continue;
                sb.Append(textLine.TextLine);
            }

            _uiText.text = sb.ToString();
        }

        private void UpdateSprites()
        {
            var elapsed = Time.time - _startTime;
            
            foreach (var controller in _controllers)
            {
                controller.Update(elapsed);
            }
        }

        public bool ForceComplete { get; set; }
    }
}