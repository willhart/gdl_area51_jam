﻿// /** 
//  * SpriteDescription.cs
//  * Will Hart
//  * 20161001
// */

namespace Assets.Core.Data
{
    #region Dependencies

    using System;
    using UnityEngine;

    #endregion

    [Serializable]
    public class SpriteDescription
    {
        public string SpriteName;
        public Vector3 InitialPosition;
        public Vector3 FinalPosition;

        public float InitialRotation;
        public float FinalRotation;

        public Sprite GameSprite;

        public float DisplayStartTime;
        public float AnimationStartTime;
        public float AnimationEndTime;
        public float DisplayEndTime;

        public float FadeInDuration;
        public float FadeOutDuration;
    }
}