﻿// /** 
//  * ChartDisplay.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core.MiniGame
{
    #region Dependencies

    using UnityEngine;

    #endregion

    [RequireComponent(typeof(LineRenderer))]
    public class ChartDisplay : MonoBehaviour
    {
        [SerializeField]
        private float _scale;
        [SerializeField]
        private float _xOffset;
        [SerializeField]
        private float _yOffset;

        private static readonly float[] BaseChartDataA = {
            0, 0, 1, 3, 2, 1, 0, 1, 3, 5, 7, 6, 5, 3, 35, 12, 0, 0, 1, 3, 0, 0, 0, 1, 2, 5, 1, 0, 0, 0
        };

        private static readonly float[] BaseChartDataB = {
            0, 1, 5, 0, 2, 1, 0, 1, 3, 5, 5, 7, 6, 12, 30, 3, 0, 0, 1, 3, 0, 1, 2, 5, 0, 0, 1, 0, 0, 0
        };

        private static readonly float[] PulsarChartDataA = {
            0, 0, 1, 3, 2, 1, 0, 1, 3, 5, 7, 6, 5, 3, 35, 12, 0, 0, 1, 3, 7, 21, 6, 1, 2, 5, 1, 0, 0, 0
        };

        private static readonly float[] PulsarChartDataB = {
            0, 1, 5, 0, 2, 1, 0, 1, 3, 5, 5, 7, 6, 12, 30, 3, 0, 0, 1, 3, 18, 9, 2, 5, 0, 0, 1, 0, 0, 0
        };

        private static readonly float[] AlienChartDataA = {
            0, 0, 7, 3, 2, 15, 0, 1, 32, 5, 7, 16, 5, 3, 35, 12, 0, 10, 1, 3, 20, 0, 5, 11, 2, 15, 1, 7, 3, 0
        };

        private static readonly float[] AlienChartDataB = {
            0, 0, 7, 3, 2, 13, 0, 19, 12, 5, 7, 6, 15, 3, 30, 3, 0, 7, 14, 3, 20, 0, 15, 1, 2, 5, 21, 7, 3, 0
        };

        private static readonly int NumPoints = 30;

        private LineRenderer _lineRenderer;

        public float Strength { private get; set; }
        public StarType StarType { get; set; }

        private void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
            Strength = 0;
            SetPoints(_lineRenderer, Strength, StarType);
        }

        public void Update()
        {
            SetPoints(_lineRenderer, Strength, StarType);
        }

        private void SetPoints(LineRenderer rend, float strength, StarType star)
        {
            var dataA = star == StarType.Normal
                ? BaseChartDataA
                : (star == StarType.Alien ? AlienChartDataA : PulsarChartDataA);
            var dataB = star == StarType.Normal
                ? BaseChartDataB
                : (star == StarType.Alien ? AlienChartDataB : PulsarChartDataB);

            var cd = Random.value > 0.5 ? dataA : dataB;

            for (var i = 0; i < NumPoints; ++i)
            {
                rend.SetPosition(i, new Vector3(i * _scale + _xOffset, _scale * (cd[i] * strength + Random.value) + _yOffset, 0));
            }
        }
    }
}