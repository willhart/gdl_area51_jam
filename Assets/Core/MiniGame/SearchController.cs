﻿// /** 
//  * SearchController.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core
{
    #region Dependencies

    using System;
    using System.Collections;
    using System.Linq;
    using Assets.Core.MiniGame;
    using UnityEngine;
    using UnityEngine.UI;

    #endregion

    public class SearchController : MonoBehaviour
    {
        [SerializeField] private float _cursorSpeed;
        [SerializeField] private float _searchPeriod;
        [SerializeField] private float _searchRadius;

        [SerializeField] private GameObject _cursor;
        [SerializeField] private SearchAudioGenerator _audioGenerator;
        [SerializeField] private GalaxyPlacer _galaxyPlacer;
        [SerializeField] private ChartDisplay _chartDisplay;
        [SerializeField] private GameObject _searchAnalysingPanel;
        [SerializeField] private GameObject _searchResultsPanel;

        private float _lastSearchTime;
        private bool _isCheckingSpectrum;

        private void FixedUpdate()
        {
            _searchAnalysingPanel.SetActive(_isCheckingSpectrum);

            if (_isCheckingSpectrum) _searchResultsPanel.SetActive(false);

            if (_isCheckingSpectrum) return;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                _cursor.transform.position += Vector3.left*_cursorSpeed*Time.fixedDeltaTime;
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                _cursor.transform.position += Vector3.right*_cursorSpeed*Time.fixedDeltaTime;
            }

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                _cursor.transform.position += Vector3.up*_cursorSpeed*Time.fixedDeltaTime;
            }
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                _cursor.transform.position += Vector3.down*_cursorSpeed*Time.fixedDeltaTime;
            }

            if (!Input.GetKeyDown(KeyCode.Space)) return;

            var stars =
                Physics2D.OverlapCircleAll(_cursor.transform.position, 5);

            if (stars.Length == 0) return;

            var star = stars.Length == 1
                ? stars[0]
                : stars.OrderBy(o => Vector3.Distance(o.transform.position, _cursor.transform.position)).First();

            if (_galaxyPlacer.HasViewedEnoughStars)
            {
                Debug.Log("Minigame complete on search");
                GameObject.FindGameObjectWithTag("GameRoot").GetComponent<Bootstrapper>().ReturnFromMiniGame();
                return;
            }
            StopAllCoroutines();
            StartCoroutine(GetStarSpectrum(star.gameObject));
        }

        private IEnumerator GetStarSpectrum(GameObject star)
        {
            _galaxyPlacer.ViewStar(star.GetComponent<StarData>());
            _isCheckingSpectrum = true;
            yield return new WaitForSeconds(1.5f);

            StartCoroutine(HandleSpectrumResult(star));
            _isCheckingSpectrum = false;
        }

        private IEnumerator HandleSpectrumResult(GameObject star)
        {
            var starData = star.GetComponent<StarData>();
            _searchResultsPanel.SetActive(true);

            var textUsed = string.Empty;

            if (_galaxyPlacer.HasViewed(star))
            {
                textUsed = _galaxyPlacer.PreviousText(star);
            }

            if (textUsed == string.Empty)
            {
                var textOptions = StarData.StarMessages[starData.TypeOfStar];
                textUsed = textOptions[UnityEngine.Random.Range(0, textOptions.Count)];
            }

            _galaxyPlacer.SetText(starData, textUsed);
            _searchResultsPanel.GetComponentInChildren<Text>().text = textUsed;
            yield return new WaitForSeconds(6f);
            _searchResultsPanel.SetActive(false);

            if (!_galaxyPlacer.HasViewedEnoughStars) yield break;

            Debug.Log("Minigame complete");
            GameObject.FindGameObjectWithTag("GameRoot").GetComponent<Bootstrapper>().ReturnFromMiniGame();
        }

        private void Update()
        {
            var now = Time.time;
            if (now < _lastSearchTime + _searchPeriod) return;

            var stars =
                Physics2D.OverlapCircleAll(_cursor.transform.position, _searchRadius);

            var hasStars = stars.Any();
            var nearestStar = hasStars
                ? stars.OrderBy(o => (o.transform.position - _cursor.transform.position).sqrMagnitude).First()
                : null;
            var starDist = hasStars
                ? (nearestStar.transform.position - _cursor.transform.position).sqrMagnitude
                : 0;

            _audioGenerator.Strength = GetSignalStrength(starDist);
            _chartDisplay.StarType = hasStars ? nearestStar.GetComponent<StarData>().TypeOfStar : StarType.Normal;
            _chartDisplay.Strength = _audioGenerator.Strength;

            _lastSearchTime = now;
        }

        private static float GetSignalStrength(float starDist)
        {
            if (Math.Abs(starDist) < float.Epsilon) return 0;

            var starSound = 1 - Mathf.Clamp01(starDist/225f);
            return starSound;
        }
        
    }
}