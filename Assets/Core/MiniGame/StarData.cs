﻿// /** 
//  * StarData.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core.MiniGame
{
    #region Dependencies

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    #endregion

    public enum StarType
    {
        Normal,
        Pulsar,
        Alien
    }

    internal class StarData : MonoBehaviour
    {
        // TODO, make more messages for stars to increase interest
        public static readonly Dictionary<StarType, List<string>> StarMessages = new Dictionary<StarType, List<string>>
        {
            { StarType.Normal, new List<string>
                {
                    "A regular blue giant. Hot, and heavy and probably lifeless.",
                    "A blue giant. Looks to be over 20,000 degrees on the surface.",
                    "You found a main sequence star.",
                    "A star not too different to ours. No strange radio signals though.",
                    "An orange star, relatively cold, but a possible candidate for life.",
                    "A little white dwarf, almost too faint to pick up. Too cold for most life forms.",
                    "A red supergiant! These are very rare. One day this may form a black hole.",
                    "A colder blue star, still many times hotter than our sun. No strange signals.",
                    "A regular main sequence star, nothing unusual here.",
                    "A main sequence star, no unusual signals."
                }
            },
            { StarType.Pulsar, new List<string>
                {
                    "Its a neutron star!",
                    "You've found a pulsar!",
                    "The twin spectra peaks suggest this is a pulsar!",
                    "Looks like a pulsar, what a find!",
                    "A slow spinning pulsar, looks relatively old. Interesting!",
                    "I think this may be a magnetar!"
                }
            },
            { StarType.Alien, new List<string>
                { "What on earth is that? I'd get on the phone to NASA, NORAD, the CIA and anybody who will listen. NOW!" }
            }
        };

        public StarType TypeOfStar;
        public Vector2 Velocity;

        private bool _inCoroutine;
        private bool _pulseUp;

        private SpriteRenderer _view;

        private void Awake()
        {
            _view = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            if (_inCoroutine) return;

            _inCoroutine = true;
            StartCoroutine(PulseStar(_pulseUp));
            _pulseUp = !_pulseUp;
        }

        private IEnumerator PulseStar(bool up)
        {
            var target = up ? 1 : 0.6f;
            var alpha = _view.color.a;

            while (true)
            {
                alpha = Mathf.MoveTowards(alpha, target, Random.value*0.01f);
                _view.color = new Color(_view.color.r, _view.color.g, _view.color.b, alpha);

                if (Mathf.Abs(target - alpha) < 0.03f)
                {
                    break;
                }

                yield return null;
            }

            _inCoroutine = false;
        }
    }
}