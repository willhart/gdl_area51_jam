﻿// /** 
//  * GalaxyPlacer.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core.MiniGame
{
    #region Dependencies

    using System.Collections.Generic;
    using UnityEngine;

    #endregion

    public class GalaxyPlacer : MonoBehaviour
    {
        [SerializeField] private GameObject _galaxyPrefab;
        [SerializeField] private Transform _container;
        [SerializeField] private int _numberOfGalaxies;

        private static int _viewedObjectLimit = 5;

        private readonly Dictionary<StarData, string> _viewedObjects = new Dictionary<StarData, string>();
        private readonly List<StarData> _galaxies = new List<StarData>();

        // Use this for initialization
        void Awake()
        {
            var yExtent = Camera.main.orthographicSize - 30;
            var xExtent = yExtent*Screen.width/Screen.height - 20;

            for (var i = 0; i < _numberOfGalaxies; ++i)
            {
                var x = Random.Range(-xExtent, xExtent);
                var y = Random.Range(-yExtent, yExtent);
                var scale = Random.Range(0.3f, 1);

                var go = Instantiate(_galaxyPrefab, _container) as GameObject;
                go.transform.position = new Vector3(x, y, 0);
                go.transform.localScale = new Vector3(scale, scale, 1);

                var sd = go.GetComponent<StarData>();
                sd.TypeOfStar = GetRandomStarType();
                sd.Velocity = Random.insideUnitCircle * 0.5f;

                _galaxies.Add(sd);
            }
            
        }

        internal void ViewStar(StarData star)
        {
            if (!_viewedObjects.ContainsKey(star))
            {
                _viewedObjects.Add(star, string.Empty);
            }
        }

        internal void SetText(StarData star, string text)
        {
            _viewedObjects[star] = text;
        }

        private StarType GetRandomStarType()
        {
            var rand = Random.value;

            if (rand < 0.01f)
                return StarType.Alien;

            return rand < 0.2f ? StarType.Pulsar : StarType.Normal;
        }

        private void FixedUpdate()
        {
            foreach (var star in _galaxies)
            {
                var sd = star.GetComponent<StarData>();
                star.transform.position += (sd.Velocity*Time.fixedDeltaTime).ToVec3();
            }
        }

        // Too much get component but whatever
        public bool HasViewed(GameObject star)
        {
            return _viewedObjects.ContainsKey(star.GetComponent<StarData>());
        }

        public string PreviousText(GameObject star)
        {
            return _viewedObjects[star.GetComponent<StarData>()];
        }

        public bool HasViewedEnoughStars
        {
            get { return _viewedObjects.Count > _viewedObjectLimit; }
        }
    }
}