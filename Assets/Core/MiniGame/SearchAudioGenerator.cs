﻿// /** 
//  * AudioGenerator.cs
//  * Will Hart
//  * 20161002
// */

namespace Assets.Core.MiniGame
{
    #region Dependencies

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Random = System.Random;

    #endregion

    /// <summary>
    /// http://www.develop-online.net/tools-and-tech/procedural-audio-with-unity/0117433
    /// </summary>
    public class SearchAudioGenerator : MonoBehaviour
    {
        private float _increment;
        private float _phase;

        [SerializeField, ReadOnly] private float _samplingFrequency;
        [SerializeField, ReadOnly] private float Gain;
        [SerializeField, ReadOnly] private float _strength;
        [SerializeField, ReadOnly] private float _minFreq;
        [SerializeField, ReadOnly] private float _maxFreq;

        private readonly Random _rand = new Random();

        private void Awake()
        {
#if UNITY_WEBGL
            // this is necessary because webgl audio support is "poor"
            var clip = Resources.Load<AudioClip>("WebGLSounds/webgl_noise");
            var source = GetComponent<AudioSource>();
            source.clip = clip;
            source.loop = true;
            source.volume = 1;
            source.Play();
            enabled = false;
#else
            _samplingFrequency = AudioSettings.outputSampleRate;
            Gain = 0.02f;
            Strength = 0;
            _minFreq = 65;
            _maxFreq = 1000;
            StartCoroutine(FadeInAudio());
#endif
        }

        private void OnAudioFilterRead(IList<float> data, int channels)
        {
            var freq = Strength*(_maxFreq - _minFreq) + _minFreq;
            _increment = freq*2*Mathf.PI/_samplingFrequency;

            for (var i = 0; i < data.Count; i = i + channels)
            {
                _phase = _phase + _increment;
                data[i] = Gain*GetWave(_phase) + GetNoise(_phase, Strength);
                if (channels == 2) data[i + 1] = data[i];
                if (_phase > 2 * Mathf.PI) _phase = 0;
            }
        }

        private float GetNoise(float phase, float frequencyProportion)
        {
            var amplitude = 0.015f * (1-frequencyProportion);
            return amplitude*(-1 + (float) _rand.NextDouble()*2);
        }

        private static float GetWave(float phase)
        {
            return SineWave(phase);
        }

        private static float SineWave(float phase)
        {
            return Mathf.Sin(phase);
        }

        private IEnumerator FadeInAudio()
        {
            var start = Time.time;
            var source = GetComponent<AudioSource>();

            while (true)
            {
                var elapsed = 0.5f * (Time.time - start);
                source.volume = Mathf.Lerp(0, 1, elapsed);
                
                if (elapsed > 1)
                {
                    yield break;
                }

                yield return null;
            }
        }

        public float Strength
        {
            get { return _strength; }
            set { _strength = Mathf.Clamp01(value); }
        }
    }
}