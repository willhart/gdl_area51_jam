# GDLArea51Jam

**Current Version 1.1.3**

This is a short narrative game written for the Game Dev League "Area 51" contest, October 2016. See https://itch.io/jam/game-dev-league-area-51-jam/rate/89592 to play and rate!

## Credits

- Sonar noise from http://freesound.org/people/Benboncan/sounds/72218/ CC by Attribution
- Fonts used are "Coming Soon" and "VT323", licenses are in the Assets/Fonts folder


## License

Excluding the assets credited above, all code, graphics and text is released under the MIT license. See `LICENSE.md` for terms or see https://bitbucket.org/willhart/gdl_area51_jam.


## Release Notes

### Version 1.1.3

 - Fix fade between spring and summer trees in Scene 8

### Version 1.1.2

 - Fix spelling mistakes in star data
 - Add some new pulsar messages

### Version 1.1.1

- Fix incorrect shader assignment

### Version 1.1.0

- Fix sprites bugs and remove colour fade in on the last frame (default sprite shader now used instead of my custom one)
- Fix bug where the minigame could run endlessly

### Version 1.0.0

- Initial release